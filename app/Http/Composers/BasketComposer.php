<?php
namespace App\Http\Composers;
use Illuminate\View\View;
use App\Repositories\UserRepository;
use Auth;
use App\Cart;
use App\Product;
use Session;

class BasketComposer
{
    protected $users;

	public function __contruct(UserRepository $users)
	{
	$this->users = $users;
	}

	public function Compose(View $view)
	{
		    if(Auth::check()){
		         $cart_products = Cart::where('user_id',Auth::user()->id)->get();
		         $total_quantity = 0;
		         $total = 0;
			         foreach($cart_products as $key => $value){
			            $cart = [];
			            $value['product_name'] = Product::where('id',$value->product_id)->pluck('product_name')->first();
			            $value['product_price'] = Product::where('id',$value->product_id)->pluck('product_price')->first();
			            $value['product_quantity'] = $value->product_quantity;
			            $value['subtotal'] = $value['product_price']*$value->product_quantity;
			            $total_quantity += $value->product_quantity;
			            $total += $value['subtotal'];
			         }
		    }else{
		        $cart_products = Session::get('cart');
		        $total = Session::get('total');
		        $total_quantity = Session::get('total_quantity');    
		    }
	    $view->with('cart_products',$cart_products)->with('total_quantity',$total_quantity)->with('total',$total);
	}

}