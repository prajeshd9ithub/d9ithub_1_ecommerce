<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Cart;
use App\Order;
use App\Category;
use App\Wallet;
use App\Transaction;
use Redirect;
use Session;
use Auth;

class BasketController extends Controller
{
    public function add_to_basket(Request $request,$id)
    {
/*
Author: Dhaval Thakkar
Editor:
created_date:2/12/2017
updated_date:
created_description: If basket is empty then products will be added to basket in this function.Logged in user's basket will be stored in database and guest's basket will be stored in session
updated_description:
*/
    	if(Auth::check()){
    		$cart_data = Cart::where('session_id',Session::getId())->first();
            if(count($cart_data) > 0){
            	$this->update_cart($request->get('product_quantity'),$id);
            	return Redirect::back();
            }
            if(count($request->get('product_quantity')) > 0){
	        	$product = Product::find($id);
	        	$cart = [];
	        	$cart['user_id'] = Auth::user()->id;
	        	$cart['product_id'] = $product->id;
	        	$cart['product_quantity'] = $request->get('product_quantity');
	        	$cart['session_id'] = Session::getId();
	        	Cart::create($cart);
	        	Session::flash('success',"Product added to your cart..!!");
	        	return Redirect::back();
	        }else{
	        	Session::flash('error',"Please add quantity for the product..!!");
	        	return Redirect::back();
	        }

    	}else{

    		if(Session::has('cart')){
    		$this->update_cart($request->get('product_quantity'),$id);
    		return Redirect::back();
    	    }

	        if(count($request->get('product_quantity')) > 0){
	        	$product = Product::find($id);
	        	$cart[] = [
	        		'id'=>$product->id,
	        	    'product_name'=>$product->product_name,
	        	    'product_price'=>$product->product_price,
	        	    'product_quantity'=>$request->get('product_quantity'),
	        	    'subtotal'=>($request->get('product_quantity')*$product->product_price)];

	            $total_quantity = $request->get('product_quantity');
	        	$total= ($request->get('product_quantity')*$product->product_price);

	        	Session::put('cart',$cart);
	        	Session::put('total_quantity',$total_quantity );
	        	Session::put('total',$total);
	        	Session::flash('success',"Product added to your cart..!!");
	        	return Redirect::back();
	        }else{
	        	Session::flash('error',"Please add quantity for the product..!!");
	        	return Redirect::back();
	        }
	    }
    }

    public function update_cart($quantity,$id)
    {
    	/*
    	Author: Dhaval Thakkar
    	Editor:
    	created_date: 2/12/2017
    	updated_date:
    	created_description: if basket has any product then this function will update basket.If user is logged in then basket will be updated in database and for guest basket will be updated in session
    	updated_description:
    	*/
        if(Auth::check()){
	            if(count($quantity) > 0){
			        $cart = Cart::where('product_id',$id)->where('session_id',Session::getId())->first();
			        	if(isset($cart) && count($cart) > 0){
				        	$cart->product_quantity = $cart->product_quantity+$quantity;
				        	$cart->update();	
			        	}else{
				        	$product = Product::find($id);
				        	$cart = [];
				        	$cart['user_id'] = Auth::user()->id;
				        	$cart['product_id'] = $product->id;
				        	$cart['product_quantity'] = $quantity;
				        	$cart['session_id'] = Session::getId();
				        	Cart::create($cart);
			        	}
		        	Session::flash('success',"Product added to your cart..!!");
		        }else{
		        	Session::flash('error',"Please add quantity for the product..!!");
		        }
    	}else{
	       $cart = Session::get('cart');

	       foreach($cart as $key => $value)
	       {
	       	$products[] = $value['id'];
	       }

	        if(in_array($id,$products)){
		       	foreach($cart as $key => $value)
		        { 
			       	if($id == $value['id'])
			       	{
			       		$value['product_quantity'] += $quantity;
			       		$value['subtotal'] = $value['product_price']*$value['product_quantity'];
	                    unset($cart[$key]);
	                    array_push($cart,$value);
			       	}
			    }
	        }else{
	            $product = Product::find($id);
	       		$cart1 = [
	    		'id'=>$product->id,
	    	    'product_name'=>$product->product_name,
	    	    'product_price'=>$product->product_price,
	    	    'product_quantity'=>$quantity,
	    	    'subtotal'=> ($quantity*$product['product_price'])];
	          array_push($cart,$cart1);
	        }
	   
	        $total_quantity = 0;
	        $total = 0;

	        foreach($cart as $key => $value)
	        {
	        	$total_quantity += $value['product_quantity'];
	        	$total += $value['subtotal'];
	        }
	        Session::put('cart',$cart);
	        Session::put('total_quantity',$total_quantity );
	        Session::put('total',$total);
           Session::flash('success',"Product added to your cart..!!");
		}
    }

    public function basket()
    {
    	/*
    	Author: Dhaval Thakkar
    	Editor:
    	created_date: 2/12/2017
    	updated_date:
    	created_description: Basket shown in header is added from view composer so I have commentted below code.It will open basket page and will show all the products of basket
    	updated_description:
    	*/

    	/*if(Auth::check()){
         $cart_products = Cart::where('user_id',Auth::user()->id)->get();
         $total_quantity = 0;
         $total = 0;
	         foreach($cart_products as $key => $value){
	            $cart = [];
	            $value['product_name'] = Product::where('id',$value->product_id)->pluck('product_name')->first();
	            $value['product_price'] = Product::where('id',$value->product_id)->pluck('product_price')->first();
	            $value['product_quantity'] = $value->product_quantity;
	            $value['subtotal'] = $value['product_price']*$value->product_quantity;
	            $total_quantity += $value->product_quantity;
	            $total += $value['subtotal'];
	         }
        }else{
        $cart_products = Session::get('cart');
        $total = Session::get('total');
        $total_quantity = Session::get('total_quantity');    
        }

    	return view('customer.basket',compact('cart_products','total_quantity','total'));*/
    	
    	return view('customer.basket');
    }

    public function checkout(){
    	/*
    	Author: Dhaval Thakkar
    	Editor:
    	created_date: 2/12/2017
    	updated_date:
    	created_description: Click on Checkout button in basket page and it will redirect you to page where you will enter card details
    	updated_description:
    	*/
    	if(Auth::check())
    	{
    		$cart = Cart::where('user_id',Auth::user()->id)->get();
    		$total_quantity=$total=0;
    		foreach($cart as $key => $value)
    		{
    			$total_quantity += $value->product_quantity;
    			$total += (Product::where('id',$value->product_id)->pluck('product_price')->first())*$value->product_quantity;
    		}
    	}else{
            if(Session::has('cart')){
    		$cart = Session::get('cart');
    		$total_quantity = Session::get('total_quantity');
    		$total = Session::get('total');
    	    }else{
    		$cart = null;
    		$total_quantity = null;
    		$total = null;
    	    }

    	}
    	
    	return view('customer.checkout',compact('cart','total_quantity','total'));
    }

    public function proceed_checkout(Request $request) 
     {
     	/*
     	Author: Dhaval Thakkar
     	Editor:
     	created_date: 2/12/2017
     	updated_date:
     	created_description:After entering card number and expiry date, it will proceed to pay the amount or bill and if payment is made successfully then amount will be deposited in vendor(merchant) and superadmin's wallet and also listed in transaction history
     	updated_description:
     	*/
     	
	           $result = \Braintree_Transaction::sale([
			  'amount' => $request->get('amount'),
			  'paymentMethodNonce' => $request->get('payment_method_nonce'),
			  'options' => [
			    'submitForSettlement' => True
			  ]
			]);

			if ($result->success) {
			  $cart = Cart::where('user_id',Auth::user()->id)->get();
			  $order = [];
			  $order['user_id'] = Auth::user()->id;
			  $order['order_details'] = serialize($cart);
			  $order_placed = Order::create($order);
			   if($order_placed)
			   	{
			   		Cart::where('user_id',Auth::user()->id)->delete();
			   		//below code will update the wallet and transaction history & user does payment
			   		$order_details = unserialize($order_placed->order_details);
			          foreach($order_details as $k => $v)
			          {	              
			                $vendor_id = Product::where('id',$v->product_id)->pluck('vendor_id')->first();

			                    $v->product_price = Product::where('id',$v->product_id)->pluck('product_price')->first();
			                    $v->product_total = ($v->product_quantity)*($v->product_price);
			                    $category_id = Product::where('id',$v->product_id)->pluck('category_id')->first();
			                    $deduction = Category::where('id',$category_id)->pluck('deduction')->first();
			                    $v->deduction = ($v->product_total*$deduction)/100;
			                    $v->paid_to_vendor = ($v->product_total)-($v->deduction);
			                   
			                    $superadmin = Wallet::where('user_id',1)->first();
			                    if(isset($superadmin) && count($superadmin) > 0){
                                 $superadmin->balance += $v->deduction;
                                 $superadmin->update();
			                    }else{	
			                    Wallet::create(['user_id'=>1,'balance'=>$v->deduction]);
			                    }
			                    Transaction::create(['wallet_id'=>1,'amount'=>$v->deduction,'type'=>'deposit']); 

			                    $vendor = Wallet::where('user_id',$vendor_id)->first();
			                    if(isset($vendor) && count($vendor) > 0){
                                 $vendor->balance += $v->paid_to_vendor;
                                 $vendor->update();
			                    }else{	
			                    Wallet::create(['user_id'=>$vendor_id,'balance'=>$v->paid_to_vendor]);
			                    }
			                    Transaction::create(['wallet_id'=>$vendor_id,'amount'=>$v->paid_to_vendor,'type'=>'deposit']);    			              
			          }
			          //ends code for wallet and transaction
			   	}
			  Session::flash('success','Payment made successfully');
			  return Redirect::to('/');
			} else {
			  Session::flash('error','Error in payment transaction');
			  return Redirect::back();
			}
     }

    public function delete($id)
    {
    	/*
    	Author: Dhaval Thakkar
    	Editor:
    	created_date: 2/12/2017
    	updated_date:
    	created_description: This function will remove products from basket and also from database if user is logged in else it will remove from session
    	updated_description:
    	*/
     	if(Auth::check())
 		{
         Cart::where('user_id',Auth::user()->id)->where('product_id',$id)->delete();
         Session::flash('success','Product removed from your Basket');
         return Redirect::back();
 		}else{
 			$cart = Session::get('cart');
 			foreach($cart as $key => $value)
 			{
 				if($value['id'] == $id)
 				{
 					unset($cart[$key]);
 				}
 			}
 			$total_quantity = 0;
 			$total = 0;
 			if(isset($cart) && count($cart) > 0)
 			{
                foreach($cart as $key => $value)
 			    {
	 			$total_quantity += $value['product_quantity'];
    			$total += (Product::where('id',$value['id'])->pluck('product_price')->first())*$value['product_quantity'];
 			    }
 			    Session::put('cart',$cart);
 			    Session::put('total_quantity',$total_quantity);
 			    Session::put('total',$total);
 			}else{
 				$total_quantity = 0;
 				$total = 0;
 			    Session::flush();
 			}
 			
 			
         Session::flash('success','Product removed from your Basket');
         return Redirect::back();
 		}
    }
}
