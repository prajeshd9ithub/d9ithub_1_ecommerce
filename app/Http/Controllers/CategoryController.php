<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;
use Redirect;
use Session;

class CategoryController extends Controller
{

    public function index()
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: this will list all categories
        updated_description:
        */
    	$categories = Category::get();
    	return view('manage.category.list',compact('categories'));
    }

    public function create()
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: this will create category
        updated_description:
        */
        return view('manage.category.category');
    }

    public function store(Request $request)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: this will store category
        updated_description:
        */
    	$input = [];
    	$input['category_name'] = $request->get('category_name');
        $input['deduction'] = $request->get('deduction');
        $category = Category::create($input);

        return Redirect::to('manage/category/'.$category->id.'/subcategory/list');
    }

    public function edit($id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: this will edit category
        updated_description:
        */
    	$category = Category::find($id);
    	return view('manage.category.category',compact('category'));
    }

    public function update(Request $request,$id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: this will update category
        updated_description:
        */
    	$category = Category::find($id);
        $category->category_name = $request->get('category_name');
        $category->deduction = $request->get('deduction');
        $category->update();

    	return Redirect::to(route('manage.category'));
    }

    public function delete($id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: this will delete category
        updated_description:
        */
    	$category = Category::find($id);
    	$category->delete();
    	return Redirect::to(route('manage.category'));
    }

    public function create_subcategory($id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: this will create subcategory
        updated_description:
        */
        $category = Category::where('id',$id)->first();
        return view('manage.category.subcategory',compact('category'));
    }

    public function edit_subcategory($cid,$id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: this will edit subcategory
        updated_description:
        */
        $subcategory = SubCategory::where('id',$id)->first();
        return view('manage.category.subcategory',compact('subcategory','cid'));
    }

    public function list_subcategory($id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: this will list subcategory
        updated_description:
        */
        $subcategories = Subcategory::where('category_id',$id)->get();
        return view('manage.category.list-subcategory',compact('subcategories','id'));
    }

    public function delete_subcategory($cid,$id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: this will delete subcategory
        updated_description:
        */
        Subcategory::where('category_id',$cid)->where('id',$id)->delete();
        Session::flash('success','Subcategory deleted successfully');
        return Redirect::back();
    }

    public function store_subcategory(Request $request,$id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: this will store subcategory
        updated_description:
        */
        $subcategories = Subcategory::where('category_id',$id)->get();
        $input = [];
        $input['name'] = $request->get('name');
        $input['category_id'] = $id;
        SubCategory::create($input);
        Session::flash('success','Subcategory created successfully');
        return Redirect::to('manage/category/'.$id.'/subcategory/list');
    }

    public function update_subcategory(Request $request,$cid,$id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: this will update subcategory
        updated_description:
        */
        $subcategory = Subcategory::where('id',$id)->update(['name'=>$request->get('name')]);
        Session::flash('success','Subcategory updated successfully');
        return Redirect::to('manage/category/'.$cid.'/subcategory/list');
    }
}
