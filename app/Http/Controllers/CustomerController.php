<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Cart;
use Session;
use Auth;
use App\User;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will redirect to landing page(products page) , front-end side
        updated_description:
        */
        $products = Product::get();
        if(Auth::check()){
         $cart_products = Cart::where('user_id',Auth::user()->id)->get();
         $total_quantity = 0;
         foreach($cart_products as $key => $value){
            $total_quantity += $value->product_quantity;
         }
        }else{
        $total_quantity = Session::get('total_quantity');    
        }
        foreach($products as $key => $value)
        {
            $value->product_by = User::where('id',$value->created_by)->pluck('name')->first();
        }
        return view('customer.products',compact('products','total_quantity'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
