<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscriptions;
use Auth;
use Redirect;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: Earlier it was redirected to /home url after user logged in but now after checking if the user is superadmin,admin,manager or customer(normal user) , we are redirecting them to specific pages according to below conditions
        updated_description:
        */
        $user = User::find(Auth::user()->id);

        if(Auth::check() && ($user->hasRole('manager')))
        {
            $subscription = Subscriptions::where('user_id',Auth::user()->id)->orderBy('created_at','DESC')->first();
            if(!isset($subscription) || $subscription->ends_at == null)
            {
                return Redirect::to('plans');
            }
            if(strtotime($subscription->ends_at) < strtotime(date('Y/m/d H:i:s')))
            {
            return view('home');
            }else{
            return Redirect::to('manage');  
            }
        }elseif(Auth::check() && ($user->hasRole('admin') || $user->hasRole('superadmin'))){
            return Redirect::to('manage');
        }else{
            return Redirect::to('/');
        }
        
    }
}
