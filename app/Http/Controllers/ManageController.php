<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Product;
use App\Category;
use App\Wallet;
use App\Transaction;
use App;
use Redirect;
use Session;
use Auth;

class ManageController extends Controller
{
    public function dashboard()
    {
      /*
      Author: Dhaval Thakkar
      Editor:
      created_date: 2/12/2017
      updated_date:
      created_description: Backend dashboard
      updated_description:
      */
    	return view('manage.dash');
    }   

    public function change_language(Request $request)
    {
      /*
      Author: Dhaval Thakkar
      Editor:
      created_date: 2/12/2017
      updated_date:
      created_description: This will change the language
      updated_description:
      */
      App::setLocale($request->get('language'));
      Session::flash('success','Language successfully changed');
      return Redirect::back();
    }

    public function orders()
    {
      /*
      Author: Dhaval Thakkar
      Editor:
      created_date: 2/12/2017
      updated_date:
      created_description: Superadmin will able to see all orders placed and manager will see only his/her products' orders
      updated_description:
      */
      $vendor_orders = [];
      $orders = Order::get();
      foreach($orders as $key => $value)
      {
          $order_details = unserialize($value->order_details);
          foreach($order_details as $k => $v)
          {
              if(Auth::user()->hasRole('superadmin'))
              {
                $v->product_name = Product::where('id',$v->product_id)->pluck('product_name')->first();
                $v->product_price = Product::where('id',$v->product_id)->pluck('product_price')->first();
                $v->product_total = ($v->product_quantity)*($v->product_price);
                $category_id = Product::where('id',$v->product_id)->pluck('category_id')->first();
                $deduction = Category::where('id',$category_id)->pluck('deduction')->first();
                $v->deduction = ($v->product_total*$deduction)/100;
                $v->category_name = Category::where('id',$category_id)->pluck('category_name')->first();
                $v->placed_on = date("m/d/Y H:i:s",strtotime($v->created_at));
                $vendor_orders[] = $v;
              }else{
                $vendor_id = Product::where('id',$v->product_id)->pluck('vendor_id')->first();
                if(Auth::user()->id == $vendor_id)
                  {
                    $v->product_name = Product::where('id',$v->product_id)->pluck('product_name')->first();
                    $v->product_price = Product::where('id',$v->product_id)->pluck('product_price')->first();
                    $v->product_total = ($v->product_quantity)*($v->product_price);
                    $category_id = Product::where('id',$v->product_id)->pluck('category_id')->first();
                    $deduction = Category::where('id',$category_id)->pluck('deduction')->first();
                    $v->deduction = ($v->product_total*$deduction)/100;
                    $v->category_name = Category::where('id',$category_id)->pluck('category_name')->first();
                    $v->placed_on = date("m/d/Y H:i:s",strtotime($v->created_at));
                    $vendor_orders[] = $v;
                  }
                  
              }
          }
      }
       return view('manage.orders',compact('vendor_orders'));
    }

    public function wallet()
    {
      /*
      Author: Dhaval Thakkar
      Editor:
      created_date: 2/12/2107
      updated_date:
      created_description: This will show current balance and all transaction history
      updated_description:
      */
      $wallet = Wallet::where('user_id',Auth::user()->id)->first();
      $i = 0;
      $transactions = Transaction::where('wallet_id',$wallet->id)->get();
      foreach($transactions as $key => $value)
      {
        $value->serial_no = ++$i;
        $value->transaction_made_on = date('m/d/Y H:i:s',strtotime($value->created_at));
      }

       return view('manage.wallet',compact('wallet','transactions'));
    }
   
}
