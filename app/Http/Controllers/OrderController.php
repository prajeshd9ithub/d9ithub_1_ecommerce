<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Product;
use App\Cart;
use Auth;

class OrderController extends Controller
{
    public function orders()
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: Frontend users will see all orders they placed so far
        updated_description:
        */
    	$products = Product::get();
        if(Auth::check()){
         $cart_products = Cart::where('user_id',Auth::user()->id)->get();
         $total_quantity = 0;
         foreach($cart_products as $key => $value){
            $total_quantity += $value->product_quantity;
         }
        }else{
        $total_quantity = Session::get('total_quantity');    
        }

    	$orders = Order::where('user_id',Auth::user()->id)->get();
    	$i = 0;
    	foreach($orders as $key => $value)
    	{
    		$products = unserialize($value->order_details);
    		$value->sr_no = ++$i;
    		$value->date = date('m/d/Y',strtotime($value->created_at));
    		$value->order_quantity = $value->order_total = 0;
    		foreach($products as $k => $v)
    		{
    			$value->order_quantity += $v->product_quantity;
    			$value->order_total += (Product::where('id',$v->product_id)->pluck('product_price')->first())*$v->product_quantity;
    		}

    	}
    	return view('customer.orders',compact('orders','total_quantity'));
    }
}
