<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Redirect;
use Input;
use Session;
use App\User;
use Auth;
use App\Subscriptions;
use App\Variant;
use App\Variant_option;

class ProductController extends Controller
{
    public function index()
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will list all products in backend
        updated_description:
        */
        $user = User::find(Auth::user()->id);

        if(Auth::check() && ($user->hasRole('manager')))
        {
            $subscription = Subscriptions::where('user_id',Auth::user()->id)->orderBy('created_at','DESC')->first();
            if(!isset($subscription) || $subscription->ends_at == null)
            {
                return Redirect::to('plans');
            }
        }
        if(Auth::check() && ($user->hasRole('superadmin'))){
        $products = Product::get();  
        }else{
    	$products = Product::where('created_by',Auth::user()->id)->get();     
        }
    	foreach($products as $key => $value)
    	{
    		$value->category_name = Category::where('id',$value->category_id)->pluck('category_name')->first();
            $value->vendor_name = User::where('id',$value->vendor_id)->pluck('name')->first();
    	}
    	
    	return view('manage.products.list',compact('products'));
    }

    public function create()
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: product creation page
        updated_description:
        */
    	$categories = Category::get();
        return view('manage.products.product',compact('categories'));
    }

    public function store(Request $request)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will store product
        updated_description:
        */
        
        $file = $request->file('product_image');
    	$destinationPath = public_path().'/images'; // upload path
        $extension = $file->getClientOriginalExtension(); // getting image extension
        $files=explode('.', $file->getClientOriginalName());
        $fileName = $files[0].time().'.'.$extension;
        $file->move($destinationPath,$fileName); // uploading file to given path


    	$input = [];
    	$input['encode'] = "p".str_random(10);
    	$input['product_name'] = $request->get('product_name');
    	$input['product_description'] = $request->get('product_description');
    	$input['product_image'] = $fileName;
    	$input['category_id'] = $request->get('category_id');
    	$input['stock'] = $request->get('stock');
        $input['product_price'] = $request->get('product_price');
        $input['created_by'] = Auth::user()->id;
        $input['vendor_id'] = Auth::user()->id;

        $product = Product::create($input);
        
        

        return Redirect::to(route('manage.products.attributes',[$product->id]));
    }

    public function edit($id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will edit product
        updated_description:
        */
    	$product = Product::find($id);
    	$categories = Category::get();
    	return view('manage.products.product',compact('product','categories'));
    }

    public function update(Request $request,$id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will update product
        updated_description:
        */

        $file = $request->file('product_image');
        $destinationPath = public_path().'/images'; // upload path
        $extension = $file->getClientOriginalExtension(); // getting image extension
        $files=explode('.', $file->getClientOriginalName());
        $fileName = $files[0].time().'.'.$extension;
        $file->move($destinationPath,$fileName); // uploading file to given path

    	$product = Product::find($id);

        $product->product_name = $request->get('product_name');
        $product->product_description = $request->get('product_description');
        $product->product_image = $fileName;
        $product->category_id = $request->get('category_id');
        $product->stock = $request->get('stock');
        $product->product_price = $request->get('product_price');
        $input['updated_by'] = Auth::user()->id;
        $product->vendor_id = Auth::user()->id;

        $product->update();

    	return Redirect::to(route('manage.products.attributes',[$id]));

    }

    public function delete($id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will delete product
        updated_description:
        */
    	$product = Product::find($id);
    	$product->delete();
    	return Redirect::to(route('manage.products'));
    }

    public function view_csv(Request $request)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will allow user to map CSV file
        updated_description:
        */
        $url = $request->get('url');
        $file = explode('.',$url);
        if(!(count($file) > 1) || (count($file) > 1 && $file[1] != "csv")){
         Session::flash('error','Enter URL where your CSV file is located');
         return Redirect::back();
        }
        $data = file_get_contents($request->get('url'));
        $rows = explode("\n",$data);
        $products = array();
        foreach($rows as $row) {
            $products[] = str_getcsv($row);
        }
        $file_row = array_shift($products);
        $first_record = array_shift($products);
        if(count($first_record) < 1)
        {
            Session::flash('error','Record Not Found');
            return Redirect::back();
        }

        $database_columns = ['product_id','product_name','product_description','product_price','category_id','stock','vendor_id','product_tags','product_size','product_color','product_discount','product_weight'];

        return view('manage.products.import',compact('database_columns','file_row','first_record','url'));
    }

    public function upload_products_csv(Request $request)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will store csv file products into database
        updated_description:
        */

        $data = file_get_contents($request->get('url'));
        $rows = explode("\n",$data);
        $products = array();
        foreach($rows as $row) {
            $products[] = str_getcsv($row);
        }
        $csv_column = array_shift($products);
        array_pop($products);
        
        for($i=0;$i<count($request->get('column_name'));$i++)
        {
            $p[$i] = $request->get('column_name')[$i];
        }

        if(count($products) > 0)
        {
            foreach($products as $key => $value)
            {
                
                $product = [];
                for($i=0;$i<count($request->get('column_name'));$i++)
                {
                    $p[$i] = $request->get('column_name')[$i];
                    $product[$p[$i]] = $value[$i];
                }

                Product::create($product);
            }
        }
        Session::flash('success','Products Added to database successfully');
        return Redirect::back();
    }

    public function multidelete(Request $request)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will multidelete products
        updated_description:
        */
        $products = $request->get('delete_all');
        foreach($products as $key => $value)
        {
            Product::where('id',$value)->delete();
        }
        Session::flash('success','Products deleted successfully');
        return Redirect::back();
    }

    public function view_product($user_id,$id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will show an individual product on frontend side
        updated_description:
        */
        $product = Product::where('id',$id)->first();
        $variants = Variant::where('product_id',$id)->get();
        foreach($variants as $key => $value)
        {
            $value->attribute_values = Variant_option::where('product_id',$id)->where('variant_id',$value->id)->pluck('name')->toArray();
        }
        return view('customer.product',compact('product','variants'));
    }

    public function view_customer_products($user_id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will show all products related to one vendor(an individual vendor page)
        updated_description:
        */

        $products = Product::where('created_by',$user_id)->get();
        $provided_by = User::where('id',$user_id)->pluck('name')->first();
        return view('customer.user-products',compact('products','provided_by'));
    }

    public function add_attributes($id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will open form to add attributes to prodoct in backend
        updated_description:
        */
        $product = Product::where('id',$id)->first();
        $variant = Variant::where('product_id',$id)->first();
        return view('manage.products.attributes',compact('product','variant'));
    }

    public function store_attributes(Request $request,$id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will store product attributes(size,color..)
        updated_description:
        */
        $attribute = [];
        $attribute['product_id'] = $id;
        $attribute['name'] = strtolower($request->get('attribute_name'));
        $variant = Variant::create($attribute);

        $attribute_values = explode(',',$request->get('attribute_values'));
        foreach($attribute_values as $key => $value)
        {
            if(isset($value) && $value != "")
            {
                $attribute_value = [];
                $attribute_value['product_id'] = $id;
                $attribute_value['variant_id'] = $variant->id;
                $attribute_value['name'] = strtolower($value);
                Variant_option::create($attribute_value);
            }
        }
     Session::flash('success','Product attributes added successfully');
     return Redirect::back();
    }

    public function list_attributes($id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will list product attributes(related to one product)
        updated_description:
        */
        $attributes = Variant::where('product_id',$id)->get();
        foreach($attributes as $key => $value)
        {
            $value->product_name = Product::where('id',$id)->pluck('product_name')->first();
            $value->attribute_name = $value->name;
            $variant_options = implode(',',Variant_option::where('variant_id',$value->id)->pluck('name')->toArray());
            $value->attribute_values = $variant_options;
        }
        return view('manage.products.attributes-list',compact('attributes','id'));
    }

    public function edit_attributes($product_id,$attribute_id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will edit attributes to related one product
        updated_description:
        */
        $product = Product::where('id',$product_id)->first();
        $attributes = Variant_option::where('product_id',$product_id)->where('variant_id',$attribute_id)->get();
        $attribute = Variant::where('id',$attribute_id)->first();
        foreach($attributes as $key => $value)
        {
           $options[] = $value->name;
        }
        $attribute->attribute_values = implode(',',$options);
        $attribute->attribute_name = Variant::where('id',$attribute_id)->pluck('name')->first();
       
        return view('manage.products.attributes',compact('attribute','product'));
    }

    public function update_attributes(Request $request,$product_id,$attribute_id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will update attributes of a product
        updated_description:
        */
        $variants = Variant_option::where('product_id',$product_id)->where('variant_id',$attribute_id)->get();
        foreach($variants as $key => $value)
        {
           $options[] = $value->name;
        }
        $attributes = explode(',',$request->get('attribute_values'));
        foreach($attributes as $key => $value)
        {
            if(isset($value) && $value != "")
            {
                if(in_array($value,$options))
                {
                    continue;
                }else{
                    $input = [];
                    $input['product_id'] = $product_id;
                    $input['variant_id'] = $attribute_id;
                    $input['name'] = $value;
                    Variant_option::create($input);
                }
            }
        }
        foreach($options as $key => $value)
        {
            if(in_array($value,$attributes))
            {
                continue;
            }else{
                Variant_option::where('product_id',$product_id)->where('variant_id',$attribute_id)->where('name',$value)->delete();
            }
        }
     Session::flash('success','Product attributes updated successfully');
     return Redirect::back();
    }

    public function delete_attributes($product_id,$attribute_id)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will delete product attribute
        updated_description:
        */
        $variant = Variant::where('product_id',$product_id)->where('id',$attribute_id)->delete();
        if($variant){
            Variant_option::where('product_id',$product_id)->where('variant_id',$attribute_id)->delete();
        }
        Session::flash('success','Attribute deleted successfully');
        return Redirect::back();

    }
}
