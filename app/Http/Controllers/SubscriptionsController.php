<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan;
use App\Subscriptions;
use Auth;
use Carbon\Carbon;

class SubscriptionsController extends Controller
{
     public function store(Request $request)
    {
          // get the plan after submitting the form
          $plan = Plan::findOrFail($request->plan);

          // subscribe the user
          $request->user()->newSubscription('main', $plan->braintree_plan)->create($request->payment_method_nonce);
/*
Author: Dhaval Thakkar
Editor:
created_date: 2/12/2017
updated_date:
created_description: Below code will add active subscription time
updated_description:
*/
          $subscription = Subscriptions::where('user_id',Auth::user()->id)->orderBy('created_at','DESC')->first();
          if($plan->braintree_plan == "monthly")
          {
             $subscription->ends_at = Carbon::now()->addDays(30);
             $subscription->save();
          }
          if($plan->braintree_plan == "yearly")
          {
             $subscription->ends_at = Carbon::now()->addDays(365);
             $subscription->save();
          }

          // redirect to home after a successful subscription
          return redirect('home');
    }
}
