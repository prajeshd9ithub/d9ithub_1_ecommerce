<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\manager_details;
use Redirect;
use Auth;
use Session;
use Hash;
use DB;
use Carbon\Carbon;

class UserController extends Controller
{
    public function index()
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: This will list all admins and managers
        updated_description:
        */
        $user_id = DB::table('role_user')->whereIn('role_id',[2,3])->get()->pluck('user_id');
        $users = User::whereIn('id',$user_id)->get();
        $withdraw_requests = manager_details::where('withdraw_request',1)->get();
        foreach($users as $key => $value)
        {
            $request = manager_details::where('user_id',$value->id)->where('withdraw_request',1)->first();
            if(isset($request) && count($request) > 0)
            {
            $value->request = "yes";    
            }
        }
    	return view('manage.users.users',compact('users'));
    }

    public function create(Request $request)
    {
    	return view('manage.dash');
    }

    public function register_manager()
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: registration form for those who want to be a manager
        updated_description:
        */
    	return view('manage.manager.register');
    }

    public function store_manager(Request $request)
    {
        /*
        Author: Dhaval Thakkar
        Editor:
        created_date: 2/12/2017
        updated_date:
        created_description: this will store the manager
        updated_description:
        refer link: https://developers.braintreepayments.com/reference/request/merchant-account/create/php
        */
    	$this->validate($request, [
		    'email' => 'unique:users|max:132',
		    'password' => 'min:6|max:20',
		    'confirm_password' => 'same:password',
            'date_of_birth' => 'before:'.Carbon::now(),
		]);

    	$input = [];
    	$input['name'] = $request->get('first_name')." ".$request->get('last_name');
    	$input['email'] = $request->get('email_id');
    	$input['password'] = Hash::make($request->get('password'));
        $input['mobile'] = $request->get('mobile');
        $input['date_of_birth'] = $request->get('date_of_birth');
    	$input['street_address'] = $request->get('street_address');
        $input['zipcode'] = $request->get('zipcode');
    	$input['city'] = $request->get('city');
    	$input['state'] = $request->get('state');
    	$input['country'] = $request->get('country');
    	$user = User::create($input);

        $customer = User::where('email',$request->get('email_id'))->first();
        $customer->attachRole(3);
        /*$response = \Braintree_Customer::create([
           'id' => $customer->id
        ]);

        // save your braintree customer id
        if( $response->success) {
        
            $customer->braintree_id = $response->merchantAccount->id;
            $customer->save();
        }else{
            Session::flash('error','Something went wrong, please try again');
            return Redirect::back();
        }*/

        $credentials = array(
		    'email' => $request->get('email_id'),
		    'password' => $request->get('password')
		);

		if (Auth::attempt($credentials)) {
		    return Redirect::to('plans');
		}else{
        Session::flash('error','Unable to create your account');
        return Redirect::back();
        }	
    }

    public function show($id)
    {
        /*
        Author:
        Editor:
        created_date:
        updated_date:
        created_description: It will show manager details to superadmin
        updated_description:
        */
     $user = User::where('id',$id)->first();
     return view('manage.users.show',compact('user'));
    }

    public function approve($id)
    {
        /*
        Author:
        Editor:
        created_date:
        updated_date:
        created_description: Superadmin will approve manager once the manager is registered and after that manager will be able to pay for subscription
        updated_description:
        */
       $user = User::where('id',$id)->update(['approve'=>1]);
       Session::flash('success','Manager Approved successfully');
       return Redirect::back();
    }
}
