<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $fillable = [
        'product_id', 'in_quantity', 'out_quantity','total_quantity','vendor_id','user_id'
    ];
}
