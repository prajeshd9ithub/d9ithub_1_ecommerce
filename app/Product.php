<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
       'encode','product_name', 'product_description', 'product_image','category_id','stock','created_by','updated_by','vendor_id','subcategory_id','product_price',
    ];
}
