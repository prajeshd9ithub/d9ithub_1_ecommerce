<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variant_option extends Model
{
     protected $fillable = ['name','product_id','variant_id'];
}
