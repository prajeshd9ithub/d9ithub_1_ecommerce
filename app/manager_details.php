<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class manager_details extends Model
{
    protected $fillable = [
        'user_id', 'bank_details', 'withdraw_request'
    ];
}
