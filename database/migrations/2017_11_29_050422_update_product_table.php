<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products',function(Blueprint $table){
          $table->integer('created_by');
          $table->integer('updated_by')->nullable();
          $table->integer('vendor_id')->nullable();
          $table->string('product_tags')->nullable();
          $table->string('customer_type')->nullable();
          $table->string('product_size')->nullable();
          $table->string('product_color')->nullable();
          $table->string('product_discount')->nullable();
          $table->string('product_weight')->nullable();
        });

        Schema::table('users',function(Blueprint $table){
          $table->string('address_line1')->nullable();
          $table->string('address_line2')->nullable();
          $table->string('city')->nullable();
          $table->string('state')->nullable();
          $table->string('country')->nullable();
          $table->integer('approve');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products',function(Blueprint $table){
          $table->dropColumn('created_by');
          $table->dropColumn('updated_by');
          $table->dropColumn('vendor_id');
          $table->dropColumn('product_tags');
          $table->dropColumn('customer_type');
          $table->dropColumn('product_size');
          $table->dropColumn('product_color');
          $table->dropColumn('product_discount');
          $table->dropColumn('product_weight');
        });

        Schema::table('users',function(Blueprint $table){
          $table->dropColumn('address_line1');
          $table->dropColumn('address_line2');
          $table->dropColumn('city');
          $table->dropColumn('state');
          $table->dropColumn('country');
          $table->dropColumn('approve');
        });
    }
}
