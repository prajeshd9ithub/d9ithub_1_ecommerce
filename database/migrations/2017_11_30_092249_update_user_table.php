<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function(Blueprint $table){
          $table->string('address_line1')->nullable();
          $table->string('address_line2')->nullable();
          $table->string('city')->nullable();
          $table->string('state')->nullable();
          $table->string('country')->nullable();
          $table->integer('approve')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users',function(Blueprint $table){
          $table->dropColumn('address_line1');
          $table->dropColumn('address_line2');
          $table->dropColumn('city');
          $table->dropColumn('state');
          $table->dropColumn('country');
          $table->dropColumn('approve');
        });
    }
}
