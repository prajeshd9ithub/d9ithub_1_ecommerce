<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products',function(Blueprint $table){
          $table->renameColumn('product_id','encode');
          $table->integer('created_by')->nullable();
          $table->integer('updated_by')->nullable();
          $table->integer('vendor_id')->nullable();
          $table->integer('subcategory_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('products',function(Blueprint $table){
          $table->renameColumn('encode','product_id');
          $table->dropColumn('created_by');
          $table->dropColumn('updated_by');
          $table->dropColumn('vendor_id');
          $table->dropColumn('subcategory_id');
        });
    }
}
