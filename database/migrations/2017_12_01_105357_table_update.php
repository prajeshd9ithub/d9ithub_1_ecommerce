<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('zipcode')->nullable();
            $table->bigInteger('mobile')->nullable();
        });
        Schema::table('products', function (Blueprint $table) {
            $table->bigInteger('product_price')->nullable();
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->bigInteger('tracking_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('zipcode');
            $table->dropColumn('mobile');
        });
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('product_price');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('tracking_id');
        });
    }
}
