@extends('customer.home')

@section('content')
<div class="basket">
<h1>Basket</h1>
@if(isset($cart_products))
<table class="table table-striped">
<thead>
<tr>
<td>Remove from Cart</td>
<td>Product Name</td>
<td>Product Price</td>
<td>Product Quantity</td>
<td>Product SubTotal</td>
</tr>
</thead>
<tbody>
@foreach($cart_products as $key => $value)
<tr>
@if(Auth::check())
<td><a href="{{ route('customer.basket.delete',$value['product_id']) }}"><button class="btn btn-danger">Remove Product</button></a></td>
@else
<td><a href="{{ route('customer.basket.delete',$value['id']) }}"><button class="btn btn-danger">Remove Product</button></a></td>
@endif
<td>{{ $value['product_name'] }}</td>
<td>{{ $value['product_price'] }}</td>
<td>{{ $value['product_quantity'] }}</td>
<td>{{ $value['subtotal'] }}</td>
</tr>
@endforeach
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td>Total Quantity: {{ $total_quantity }}</td>
	<td>Total Subtotal: {{ $total }}</td>
</tr>
	</tbody>
</table>
<div class="right">
	<a href="{{ url('/') }}"><button class="btn btn-default">Continue Shopping</button></a>
<a href="{{ url('checkout') }}"><button class="btn btn-default">Checkout</button></a>
</div>
@else
<h2>No Items added to your cart</h2>
@endif
</div>
@endsection