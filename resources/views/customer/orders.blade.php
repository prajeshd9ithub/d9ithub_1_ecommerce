@extends('customer.home')

@section('content')
<div class="basket">
<h1>Orders</h1>
@if(isset($orders) && $orders->count())
<table class="table table-striped">
<thead>
<tr>
<td>Serial No</td>
<td>Order Date</td>
<td>Order Quantity</td>
<td>Order Total</td>
</tr>
</thead>
<tbody>
@foreach($orders as $key => $value)
<tr>
<td>{{ $value['sr_no'] }}</td>
<td>{{ $value['date'] }}</td>
<td>{{ $value['order_quantity'] }}</td>
<td>{{ $value['order_total'] }}</td>
</tr>
@endforeach
	</tbody>
</table>
<div class="right">
	<a href="{{ url('/') }}"><button class="btn btn-default">Continue Shopping</button></a>
</div>
@else
<h2>No Order Placed Yet</h2>
@endif
</div>
@endsection