@extends('customer.home')

@section('content')
<div class="content">

    {!! Form::open(['url'=>'customer/add_to_basket/'.$product->id]) !!}
    <div class="form-group individual-product">
        <div class="individual-product-image">
        <img src="{{ asset('images') }}/{{$product->product_image}}" class="img">
         </div>
    </div>
     <div class="individual-product-info">
        <p>Name: {{$product->product_name}}</p>
        <p>Description: {{$product->product_description}}</p>
        <p>Price: {{$product->product_price}}</p>
         @foreach($variants as $key => $value)
         <p>{{$value->name}}: <select name="{{$value->name}}" required><option value>Select Any one to Order</option>@foreach($value->attribute_values as $k => $v) <option value="{{$v}}">{{$v}}</option> @endforeach </select></p>
         @endforeach
        <p>{!! Form::number('product_quantity',null,['required'])!!}</p>
        <p>{!! Form::submit('Add to Basket',['class'=>'btn btn-default']) !!}</p>
        <p><a href="{{ url('/') }}" class="btn btn-default">Continue Shopping</a></p>
     </div>
    {!! Form::close() !!}

                
</div>

@endsection