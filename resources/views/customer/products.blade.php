@extends('customer.home')

@section('content')
<div class="content">
    @foreach($products as $product)
    {!! Form::open(['url'=>'customer/add_to_basket/'.$product->id]) !!}
    <div class="form-group product">
         <a href="{{url('customer/'.$product->created_by.'/products/'.$product->id.'/view')}}"><div class="product-image">
          <img src="{{ asset('images') }}/{{$product->product_image}}" class="img">
         </div></a>
         <div class="product-info">
            <p>{{$product->product_name}}</p>
            <p>{{$product->product_description}}</p>
            <p>{{$product->product_price}}</p>
            <p>Product By: <a href="{{url('customer/'.$product->created_by.'/products')}}">{{$product->product_by}}</a></p>
            <p>{!! Form::number('product_quantity',null,['required'])!!}</p>
            <p>{!! Form::submit('Add to Basket',['class'=>'btn btn-default']) !!}</p>
         </div>
    </div>
    {!! Form::close() !!}
    @endforeach
                
</div>

@endsection