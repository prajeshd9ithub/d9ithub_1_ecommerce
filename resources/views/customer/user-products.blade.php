@extends('customer.home')

@section('content')
<div class="content">
    <h1>All Products By: {{$provided_by}}</h1>
    @foreach($products as $product)
    {!! Form::open(['url'=>'customer/add_to_basket/'.$product->id]) !!}
    <div class="form-group product">
         <a href="{{url('customer/'.$product->created_by.'/products/'.$product->id)}}"><div class="product-image">
          <img src="{{ asset('images') }}/{{$product->product_image}}" class="img">
         </div></a>
         <div class="product-info">
            <p>{{$product->product_name}}</p>
            <p>{{$product->product_description}}</p>
            <p>{{$product->product_price}}</p>
            <p>{!! Form::number('product_quantity')!!}</p>
            <p>{!! Form::submit('Add to Basket',['class'=>'btn btn-default']) !!}</p>
         </div>
    </div>
    {!! Form::close() !!}
    @endforeach
                
</div>

@endsection