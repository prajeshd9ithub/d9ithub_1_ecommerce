@extends('manage.app')

@section('content')
<div class="row pedding-remove">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="col-xs-2 col-sm-2 col-md-2 sidebar">
			@include('nav.side-menu')
	    </div>
		<div class="col-xs-10 col-sm-10 col-md-10 admin-body">
		 <h1>Create Category</h1>
		 @if(isset($category) && count($category) > 0)
		 {!! Form::model($category,['url'=>'manage/category/update/'.$category->id]) !!}
		 @else
		 {!! Form::open(['url'=>'manage/category/create']) !!}
		 @endif
		 <div class="form-group">
		 	<div>Category Name</div>
		 	<div>{!! Form::text('category_name',null,['class'=>'form-control','required']) !!}</div>
		 </div>
		 <div class="form-group">
		 	<div>Deduction Amount(%)</div>
		 	<div>{!! Form::number('deduction',null,['class'=>'form-control','required']) !!}</div>
		 </div>
		 	<div>
		 		@if(isset($category) && count($category) > 0)
		 		{!! Form::submit('Update',['class'=>'btn btn-default']) !!}
			 	@else
		 		{!! Form::submit('Submit',['class'=>'btn btn-default']) !!}
			    @endif
		</div><br>
		 {!! Form::close() !!}
	    </div>
    </div>
</div>
@endsection