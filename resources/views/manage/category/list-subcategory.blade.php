@extends('manage.app')

@section('content')
<div class="row pedding-remove">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="col-xs-2 col-sm-2 col-md-2 sidebar">
			@include('nav.side-menu')
	    </div>
		<div class="col-xs-10 col-sm-10 col-md-10 admin-body">
		 <h1>SubCategories</h1><a href="{{ url('manage/category/'.$id.'/subcategory/create') }}"><button class="btn btn-default">Create SubCategory</button></a>
		 @if(count($subcategories) > 0)
		 <table class="table">
		 	<thead>
		 		<tr>
		 			<td>Id</td>
		 			<td>Category Name</td>
		 			<td>Action</td>
		 		</tr>
		 	</thead>
		 	<tbody>
		    @foreach($subcategories as $subcategory)
		 		<tr>
		 			<td>{{$subcategory->id}}</td>
		 			<td>{{$subcategory->name}}</td>
		 			<td><a class="btn btn-default" href="{{ url('manage/category/'.$id.'/subcategory/'.$subcategory->id.'/edit') }}">Edit</a>&nbsp;<a class="btn btn-default"  href="{{ url('manage/category/'.$id.'/subcategory/'.$subcategory->id.'/delete') }}">Delete</a>&nbsp;</td>
		 		</tr>
		    @endforeach
		 	</tbody>
		 </table>
		 @else
		 <h3>No SubCategory created yet</h3>
		 @endif
	    </div>
    </div>
</div>
@endsection