@extends('manage.app')

@section('content')
<div class="row pedding-remove">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="col-xs-2 col-sm-2 col-md-2 sidebar">
			@include('nav.side-menu')
	    </div>
		<div class="col-xs-10 col-sm-10 col-md-10 admin-body">
		 <h1>All Category</h1><a href="{{ route('manage.category.create') }}"><button class="btn btn-default">Create Category</button></a>
		 @if(count($categories) > 0)
		 <table class="table">
		 	<thead>
		 		<tr>
		 			<td>Id</td>
		 			<td>Category Name</td>
		 			<td>Action</td>
		 		</tr>
		 	</thead>
		 	<tbody>
		    @foreach($categories as $category)
		 		<tr>
		 			<td>{{$category->id}}</td>
		 			<td>{{$category->category_name}}</td>
		 			<td><a class="btn btn-default" href="{{ url('manage/category/'.$category->id.'/edit') }}">Edit</a>&nbsp;<a class="btn btn-default"  href="{{ route('manage.category.delete',[$category->id]) }}">Delete</a>&nbsp;<a class="btn btn-default" href="{{ url('manage/category/'.$category->id.'/subcategory/list') }}">List of Subcategory</a></td>
		 		</tr>
		    @endforeach
		 	</tbody>
		 </table>
		 @else
		 <h3>No Category created yet</h3>
		 @endif
	    </div>
    </div>
</div>
@endsection