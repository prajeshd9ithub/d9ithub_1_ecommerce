@extends('manage.app')

@section('content')
<div class="row pedding-remove">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="col-xs-2 col-sm-2 col-md-2 sidebar">
			@include('nav.side-menu')
	    </div>
		<div class="col-xs-10 col-sm-10 col-md-10 admin-body">
		 <h1>Create SubCategory</h1>
		 @if(isset($subcategory) && count($subcategory) > 0)
		 {!! Form::model($subcategory,['url'=>'manage/category/'.$cid.'/subcategory/'.$subcategory->id.'/update']) !!}
		 @else
		 {!! Form::open(['url'=>'manage/category/'.$category->id.'/subcategory/store']) !!}
		 @endif
		 <div class="form-group">
		 	<div>SubCategory Name</div>
		 	<div>{!! Form::text('name',null,['class'=>'form-control','required']) !!}</div>
		 </div>
		 	<div>
		 		@if(isset($subcategory) && count($subcategory) > 0)
		 		{!! Form::submit('Update',['class'=>'btn btn-default']) !!}
			 	@else
		 		{!! Form::submit('Submit',['class'=>'btn btn-default']) !!}
			    @endif
		</div><br>
		 {!! Form::close() !!}
	    </div>
    </div>
</div>
@endsection