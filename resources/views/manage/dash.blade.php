@extends('manage.app')

@section('content')
<div class="row pedding-remove">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="col-xs-2 col-sm-2 col-md-2 sidebar">
			@include('nav.side-menu')
	    </div>
		<div class="col-xs-10 col-sm-10 col-md-10 admin-body">
		 <h1>Dashboard</h1>
		 {!! Form::open(['route'=>'manage.language']) !!}
		 <select name="language" class="form-control" required>
            <option value>Select Language</option>
            <option value="en">English</option>
            <option value="hi">Hindi</option>
         </select>
         <br>
         {!! Form::submit('Submit',['class'=>'btn btn-default']) !!}
         {!! Form::close() !!}
	    </div>
    </div>
</div>
@endsection