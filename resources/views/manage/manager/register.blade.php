@extends('customer.home')
@section('content')
{!! Form::open(['route'=>'manager.register']) !!}
{{ csrf_field() }}
<div class="manager">
<h1>Register Yourself to become a Manager</h1>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div><h3>Personal Information</h3></div>
<div class="manager-left">
         <div class="form-group">
		 	<div>First Name</div>
		 	<div>{!! Form::text('first_name',null,['class'=>'form-control','required']) !!}</div>
		 </div>
		 <div class="form-group">
		 	<div>Last Name</div>
		 	<div>{!! Form::text('last_name',null,['class'=>'form-control','required']) !!}</div>
		 </div>
		 <div class="form-group">
		 	<div>Email Id</div>
		 	<div>{!! Form::email('email_id',null,['class'=>'form-control','required']) !!}</div>
		 </div>
		 <div class="form-group">
		 	<div>Mobile Number</div>
		 	<div>{!! Form::number('mobile',null,['class'=>'form-control','required']) !!}</div>
		 </div>
		 <div class="form-group">
		 	<div>Password</div>
		 	<div>{!! Form::password('password',['class'=>'form-control','required']) !!}</div>
		 </div>
		 <div class="form-group">
		 	<div>Confirm Password</div>
		 	<div>{!! Form::password('confirm_password',['class'=>'form-control','required']) !!}</div>
		 </div>
</div>
<div class="manager-right">
	     <div class="form-group">
		 	<div>Date of Birth</div>
		 	<div>{!! Form::date('date_of_birth',null,['class'=>'form-control','required']) !!}</div>
		 </div>
		 <div class="form-group">
		 	<div>Street Address</div>
		 	<div>{!! Form::text('street_address',null,['class'=>'form-control','required']) !!}</div>
		 </div>
		 <div class="form-group">
		 	<div>Zipcode/PostalCode</div>
		 	<div>{!! Form::number('zipcode',null,['class'=>'form-control','required']) !!}</div>
		 </div>
		 <div class="form-group">
		 	<div>City</div>
		 	<div>{!! Form::text('city',null,['class'=>'form-control','required']) !!}</div>
		 </div>
		 <div class="form-group">
		 	<div>State</div>
		 	<div>{!! Form::text('state',null,['class'=>'form-control','required']) !!}</div>
		 </div>
		 <div class="form-group">
		 	<div>Country</div>
		 	<div>{!! Form::text('country',null,['class'=>'form-control','required']) !!}</div>
		 </div>
</div><br>

<div class="manager-left">
{!! Form::submit('Submit',['class'=>'btn btn-default']) !!}
</div>
</div>
{!! Form::close() !!}
@endsection
