@extends('manage.app')

@section('content')
<div class="row pedding-remove">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="col-xs-2 col-sm-2 col-md-2 sidebar">
			@include('nav.side-menu')
	    </div>
		<div class="col-xs-10 col-sm-10 col-md-10 admin-body">
		 <h1>All Orders</h1>
		 <br><br>
		 @if(count($vendor_orders) > 0)
		 <table class="table" id="example">
		 	<thead>
		 		<tr>
		 			<td>Product Id</td>
		 			<td>Product Name</td>
		 			<td>Category Name</td>
		 			<td>Placed On</td>
		 			<td>Product Quantity</td>
		 			<td>Product Price</td>
		 			<td>Product Total</td>
		 			<td>Deduction</td>
		 		</tr>
		 	</thead>
		 	<tbody>
		    @foreach($vendor_orders as $vendor_order)
		 		<tr>
		 			<td>{{$vendor_order->product_id}}</td>
		 			<td>{{$vendor_order->product_name}}</td>
		 			<td>{{$vendor_order->category_name}}</td>
		 			<td>{{$vendor_order->placed_on}}</td>
		 			<td>{{$vendor_order->product_quantity}}</td>
		 			<td>{{$vendor_order->product_price}}</td>
		 			<td>{{$vendor_order->product_total}}</td>
		 			<td>{{$vendor_order->deduction}}</td>
		 		</tr>
		    @endforeach
		 	</tbody>
		 </table>
		 <br>
		 @else
		 <h3>No Order placed yet</h3>
		 @endif
		 <br>
	    </div>
    </div>
</div>
@endsection