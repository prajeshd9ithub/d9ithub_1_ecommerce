@extends('manage.app')

@section('content')
<div class="row pedding-remove">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="col-xs-2 col-sm-2 col-md-2 sidebar">
			@include('nav.side-menu')
	    </div>
		<div class="col-xs-10 col-sm-10 col-md-10 admin-body">
		 <h1> Add Product Quantity</h1>
		 {!! Form::open(['array'=>'manage/products/'.$id.'/add_quantity']) !!}
		 <div class="form-group">
		 	<div>Add Quantity:</div>
		 	<div>{!! Form::number('product_quantity',null,['class'=>'form-control','required']) !!}
		 </div><br>
		 <div class="form-group">
		 	<div>{!! Form::submit('Submit',['class'=>'btn btn-default']) !!}</div>
		 </div>
		 {!! Form::close() !!}
	    </div>
    </div>
</div>
@endsection