@extends('manage.app')

@section('content')
<div class="row pedding-remove">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="col-xs-2 col-sm-2 col-md-2 sidebar">
			@include('nav.side-menu')
	    </div>
		<div class="col-xs-10 col-sm-10 col-md-10 admin-body ">
				
				<div>
					@if(isset($attributes) && count($attributes) > 0) 
					<h1> List of All Attributes of this Product</h1>
					
                   <table class="attributes">
                   	<thead>
                   		<tr>
                   			<td>{!! Form::label('attribute_name','Attribute Name') !!}</td>
                   			<td>{!! Form::label('attribute_values','Attribute Values') !!}</td>
                   		</tr>
                   	</thead>
                   	<tbody>
                   		@foreach($attributes as $key => $value)
                   		 <tr>
                   			<td>{{$value->attribute_name}}</td>
                   			<td>{{$value->attribute_values}}</td>
                   			<td><a href="{{url('manage/products/'.$id.'/attributes/'.$value->id.'/edit')}}" class="btn btn-default">Edit</a><a href="{{url('manage/products/'.$id.'/attributes/'.$value->id.'/delete')}}" class="btn btn-default">Delete</a></td>
                   		</tr>
                   		@endforeach
                   	</tbody>
                   </table>
					@else
					<h1>No Attributes added for this product</h1>
					@endif
				</div>
		<div>
		  <a href="{{url('manage/products/'.$id.'/attributes')}}" class="btn btn-default">Add Attributes</a>
		</div>

	    </div>
    </div>
</div>
@endsection