@extends('manage.app')

@section('content')
<div class="row pedding-remove">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="col-xs-2 col-sm-2 col-md-2 sidebar">
			@include('nav.side-menu')
	    </div>
		<div class="col-xs-10 col-sm-10 col-md-10 admin-body ">
		 <h1> Add Attributes to your Product</h1>
		 <h4>e.g. Attribute: Color, Value: Red,Green,Blue</h4>
         @if(isset($attribute) && count($attribute) > 0)
         {!! Form::model($attribute,['url'=>'manage/products/'.$product->id.'/attributes/'.$attribute->id.'/update','files'=>'true']) !!}
         @else
		 {!! Form::open(['url'=>'manage/products/attributes/store/'.$product->id,'files'=>'true']) !!}
         @endif
				<div class="input">
					{!! Form::label('attribute_name','Attribute Name') !!}
					@if(isset($attribute) && count($attribute) > 0)
					{!! Form::text('attribute_name',null,['class'=>'form-control','required','readonly']) !!}
					@else
					{!! Form::text('attribute_name',null,['class'=>'form-control','required']) !!}
					@endif
				</div>
				<div class="input">
					{!! Form::label('attribute_values','Attribute Values') !!}
					{!! Form::text('attribute_values',null,['class'=>'form-control','required']) !!}
				</div>
				 <div><br>
				 	@if(isset($attribute) && count($attribute) > 0)
				 	{!! Form::submit('Update',['class'=>'btn btn-default']) !!}
                    @else
					 {!! Form::submit('Submit',['class'=>'btn btn-default']) !!}
					@endif
		         </div>
		 {!! Form::close() !!}
		 <br><br>
		  <a href="{{url('manage/products/'.$product->id.'/attributes/list')}}"><button class="btn btn-default">View All Attributes</button></a>
	    </div>
    </div>
</div>
@endsection