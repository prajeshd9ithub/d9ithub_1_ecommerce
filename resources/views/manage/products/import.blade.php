@extends('manage.app')

@section('content')
<div class="row pedding-remove">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="col-xs-2 col-sm-2 col-md-2 sidebar">
			@include('nav.side-menu')
	    </div>
		<div class="col-xs-10 col-sm-10 col-md-10 admin-body">
		 <h1>Map Your CSV File</h1>
		 
		    {!! Form::open(['url'=>'manage/products/import']) !!}
            <table class="table">
            <thead>
            	<tr><td>Database Columns</td><td>CSV File Columns</td><td>CSV File First Record</td></tr>
            </thead>
            <tbody>
            	@if(isset($file_row) && count($file_row) > 0)
            	@foreach($file_row as $key => $value)
            	<tr>
            		<td><select name="column_name[]" class="form-control">@foreach($database_columns as $k => $v)<option value="{{$v}}" @if($key == $k) selected @endif>{{$v}}</option>@endforeach</select></td>
            		<td>{{$value}}</td>
            		<td>@foreach($first_record as $ke => $va)
            		    @if($key == $ke)
            		    {{$va}}
            		    @endif
            		    @endforeach</td>
            	</tr>
            	@endforeach
            	@endif
            </tbody>
            </table>
            <input type="hidden" name="url" value="{{$url}}">
		    {!! Form::submit('Import',['class'=>'btn btn-default']) !!}

		    {!! Form::close() !!}
	    </div>
    </div>
</div>
@endsection