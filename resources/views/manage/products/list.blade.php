@extends('manage.app')

@section('content')
<div class="row pedding-remove">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="col-xs-2 col-sm-2 col-md-2 sidebar">
			@include('nav.side-menu')
	    </div>
		<div class="col-xs-10 col-sm-10 col-md-10 admin-body">
		 <h1>All Products</h1><a href="{{ route('manage.products.create') }}"><button class="btn btn-default">Add Product</button></a>
		 <br><br>
		 @if(count($products) > 0)
		 {!! Form::open(['route'=>'manage.products.multidelete']) !!}
		 <table class="table" id="example">
		 	<thead>
		 		<tr>
		 			<td>Multiple Delete</td>
		 			<td>Encode</td>
		 			<td>Product Name</td>
		 			<td>Category Name</td>
		 			<td>Product Stock</td>
		 			@if(Auth::user()->hasRole('superadmin'))
		 			<td>Vendor Name</td>
		 			@endif
		 			<td>Action</td>
		 		</tr>
		 	</thead>
		 	<tbody>
		    @foreach($products as $product)
		 		<tr>
		 			<td><input type="checkbox" name="delete_all[]" value="{{$product->id}}"></td>
		 			<td>{{$product->encode}}</td>
		 			<td>{{$product->product_name}}</td>
		 			<td>{{$product->category_name}}</td>
		 			<td>{{$product->stock}}</td>
		 			@if(Auth::user()->hasRole('superadmin'))
		 			<td>{{$product->vendor_name}}</td>
		 			@endif
		 			<td><a href="{{ url('manage/products/'.$product->id.'/edit') }}" class="btn btn-default">View</a>&nbsp;
		 				<a  class="btn btn-default" href="{{url('manage/products/'.$product->id.'/attributes/list')}}">View All Attributes</a></td>
		 		</tr>
		    @endforeach
		 	</tbody>
		 </table>
		 {!! Form::submit('Delete Selected',['class'=>'btn btn-default']) !!}
		 {!! Form::close() !!}
		 <br>
		 @else
		 <h3>No Product created yet</h3>
		 @endif
		 <br>
		    {!! Form::open(['url'=>'manage/products']) !!}
		    <div>
		    Enter URL: {!! Form::url('url',null,['class'=>'form-control','required']) !!} <br>
		    {!! Form::submit('View',['class'=>'btn btn-default']) !!}
		    </div>
		    {!! Form::close() !!}
	    </div>
    </div>
</div>
@endsection