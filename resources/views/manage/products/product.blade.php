@extends('manage.app')

@section('content')
<div class="row pedding-remove">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="col-xs-2 col-sm-2 col-md-2 sidebar">
			@include('nav.side-menu')
	    </div>
		<div class="col-xs-10 col-sm-10 col-md-10 admin-body">
		 <h1> Add Product</h1>
		 @if(isset($product) && count($product) > 0)
		 {!! Form::model($product,['url'=>'manage/products/update/'.$product->id,'files'=>'true']) !!}
		 @else
		 {!! Form::open(['url'=>'manage/products/create','files'=>'true']) !!}
		 @endif
		 <!-- <div class="form-group">
		 	<div>Encode</div>
		 	<div>{!! Form::text('encode',null,['class'=>'form-control','required']) !!}</div>
		 </div> -->
		 <div class="form-group">
		 	<div>Product Name</div>
		 	<div>{!! Form::text('product_name',null,['class'=>'form-control','required']) !!}</div>
		 </div>
		 <div class="form-group">
		 	<div>Product Description</div>
		 	<div>{!! Form::textarea('product_description',null,['class'=>'form-control','required']) !!}</div>
		 </div>
		 <div class="form-group">
		 	<div>Product Image</div>
		 	<div>{!! Form::file('product_image',['class'=>'form-control','required']) !!}</div>
		 </div>
		 <div class="form-group">
		 	<div>Select Category</div>
		 	<div><select name="category_id" required class="form-control">
		 		<option value=>Select Category</option>
		 		@foreach($categories as $category)
		 		<option value="{{$category->id}}" @if(isset($product) && $category->id == $product->category_id) selected @endif>{{$category->category_name}}</option>
		 		@endforeach
		 	</select></div>
		 </div>
		 <div class="form-group">
		 	<div>Product Price</div>
		 	<div>{!! Form::number('product_price',null,['class'=>'form-control','required']) !!}</div>
		 </div>
		 <div class="form-group">
		 	<div>Product Stock</div>
		 	<div>{!! Form::number('stock',null,['class'=>'form-control','required']) !!}</div>
		 </div>
		 
		 <div>
		 	@if(isset($product) && count($product) > 0)
			 {!! Form::submit('Update',['class'=>'btn btn-default']) !!}
			 @else
			 {!! Form::submit('Submit',['class'=>'btn btn-default']) !!}
			@endif
	</div><br>
		 {!! Form::close() !!}
		 @if(isset($product) && count($product) > 0)
		 <a href="{{url('manage/products/'.$product->id.'/attributes/list')}}"><button class="btn btn-default">View All Attributes</button></a>
		 @endif
	    </div>
    </div>
</div>
@endsection