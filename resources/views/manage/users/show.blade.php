@extends('manage.app')

@section('content')
<div class="row pedding-remove">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="col-xs-2 col-sm-2 col-md-2 sidebar">
			@include('nav.side-menu')
	    </div>
		<div class="col-xs-10 col-sm-10 col-md-10 admin-body">
		 <h1>User Details</h1>
		 <div class="form-group">
		 	<div>Name: {{$user->name}}</div>
		 	<div>Email: {{$user->email}}</div>
		 	<div>Mobile: {{$user->mobile}}</div>
		 	<div>Address Line1: {{$user->address_line1}}</div>
		 	<div>Address Line2: {{$user->address_line2}}</div>
		 	<div>Zipcode: {{$user->zipcode}}</div>
		 	<div>City: {{$user->city}}</div>
		 	<div>State: {{$user->state}}</div>
		 	<div>Country: {{$user->country}}</div>
		 	<br>
		 	<a class="btn btn-default" href="{{URL::previous()}}">Back</a>
		 </div>
	    </div>
    </div>
</div>
@endsection