@extends('manage.app')

@section('content')
<div class="row pedding-remove">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="col-xs-2 col-sm-2 col-md-2 sidebar">
			@include('nav.side-menu')
	    </div>
		<div class="col-xs-10 col-sm-10 col-md-10 admin-body">
		 <h1>Users</h1>
		 <table class="table" id="example">
		 	<thead>
		 		<tr>
		 			<td>User Id</td>
		 			<td>User Name</td>
		 			<td>User Email</td>
		 			<td>Action</td>
		 		</tr>
		 	</thead>
		 	<tbody>
		    @foreach($users as $user)
		 		<tr>
		 			<td>{{$user->id}}</td>
		 			<td>{{$user->name}}</td>
		 			<td>{{$user->email}}</td>
		 			<td><a href="{{ url('manage/users/'.$user->id.'/show') }}" class="btn btn-default">View</a>&nbsp; 
		 				@if($user->hasRole('manager') && $user->approve != 1)<a href="{{ route('manage.users.approve',[$user->id]) }}" class="btn btn-default">Approve</a>@endif</td>
		 		</tr>
		    @endforeach
		 	</tbody>
		 </table>
	    </div>
    </div>
</div>
@endsection