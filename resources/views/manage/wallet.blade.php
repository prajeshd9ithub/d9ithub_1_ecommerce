@extends('manage.app')

@section('content')
<div class="row pedding-remove">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="col-xs-2 col-sm-2 col-md-2 sidebar">
			@include('nav.side-menu')
	    </div>
		<div class="col-xs-10 col-sm-10 col-md-10 admin-body">
		 <h1>Wallet History</h1>
		 Current Balance: @if(isset($wallet) && count($wallet) > 0) {{$wallet->balance}} @else 0 @endif
		 <br><br>
		 @if(isset($transactions) && count($transactions) > 0)
		 <table class="table" id="example">
		 	<thead>
		 		<tr>
		 			<td>Serial Number</td>
		 			<td>Transaction Amount</td>
		 			<td>Transaction Type</td>
		 			<td>Transaction made on</td>
		 		</tr>
		 	</thead>
		 	<tbody>
		    @foreach($transactions as $transaction)
		 		<tr>
		 			<td>{{$transaction->serial_no}}</td>
		 			<td>{{$transaction->amount}}</td>
		 			<td>{{$transaction->type}}</td>
		 			<td>{{$transaction->transaction_made_on}}</td>
		 		</tr>
		    @endforeach
		 	</tbody>
		 </table>
		 <br>
		 @else
		 <h3>No Transaction made yet</h3>
		 @endif
		 <br>
	    </div>
    </div>
</div>
@endsection