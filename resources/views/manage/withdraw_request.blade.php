@extends('manage.app')

@section('content')
<div class="row pedding-remove">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="col-xs-2 col-sm-2 col-md-2 sidebar">
			@include('nav.side-menu')
	    </div>
		<div class="col-xs-10 col-sm-10 col-md-10 admin-body">
				 <h1>Make a Withdraw Request</h1>
				 @if(isset($details) && count($details) > 0)
				 {!! Form::model($details,['url'=>'manage/wallet/'.$id.'/withdraw_request/update']) !!}
				 @else
				 {!! Form::open(['url'=>'manage/wallet/'.$id.'/withdraw_request']) !!}
				 @endif
				 <div class="form-group">
				 	<div>Bank name:</div>
				 	<div>{!! Form::text('bank_name',null,['class'=>'form-control','required']) !!}</div>
				 </div>
				 <div class="form-group">
				 	<div>Account Number:</div>
				 	<div>{!! Form::text('account_number',null,['class'=>'form-control','required']) !!}</div>
				 </div>
				 <div class="form-group">
				 	<div>Bank IFSC Code:</div>
				 	<div>{!! Form::text('bank_ifsc_code',null,['class'=>'form-control','required']) !!}</div>
				 </div>
				 <div class="form-group">
				 	<div>Bank address:</div>
				 	<div>{!! Form::text('bank_address',null,['class'=>'form-control','required']) !!}</div>
				 </div>
				 <div class="form-group">
				 	<div>{!! Form::submit('Submit',['class'=>'btn btn-default']) !!}</div>
				 </div>
				 {!! Form::close() !!}
	    </div>
    </div>
</div>
@endsection