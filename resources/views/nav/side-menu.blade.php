<aside class="menu">
	<p class="menu-label">General</p>
	<ul class="menu-list">
		<li><a href="{{route('manage.dashboard')}}">Dashboard</a></li>
	</ul>
	<p class="menu-label">Administration</p>
	<ul class="menu-list">
		@if(Auth::check() && Auth::user()->hasRole('superadmin'))
		<li><a href="{{route('manage.users')}}">Manage Users</a></li>
		<li><a href="{{route('manage.category')}}">Category</a></li>
		@endif
		<li><a href="{{route('manage.products')}}">Products</a></li>
		<li><a href="{{route('manage.orders')}}">Orders</a></li>
		<li><a href="{{route('manage.wallet')}}">Wallet</a></li>
	</ul>
</aside>
