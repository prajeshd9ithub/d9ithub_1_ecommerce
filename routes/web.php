<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'CustomerController@index');
Route::resource('customer','CustomerController');

Route::group(['prefix'=>'customer'],function(){
  Route::post('add_to_basket/{id}','BasketController@add_to_basket');
  Route::get('basket/{id}/view',['as'=>'customer.basket','uses'=>'BasketController@basket']);
  Route::get('delete/{id}',['as'=>'customer.basket.delete','uses'=>'BasketController@delete']);
  Route::get('{user_id}/products','ProductController@view_customer_products');
  Route::get('{user_id}/products/{id}/view','ProductController@view_product');
  Route::get('orders/{id}/view',['as'=>'customer.orders','uses'=>'OrderController@orders']);
});

Route::group(['prefix'=>'manage','middleware'=>['role:superadmin|admin|manager','auth']],function(){

     Route::get('/','ManageController@dashboard');
     Route::get('dashboard', ['as' => 'manage.dashboard', 'uses'=>'ManageController@dashboard']);
    /* Route::get('users', ['as' => 'manage.users', 'uses'=>'ManageController@users']);*/
     Route::post('change_language',['as'=>'manage.language','uses'=>'ManageController@change_language']);
     Route::get('orders', ['as' => 'manage.orders', 'uses'=>'ManageController@orders']);
     Route::get('wallet', ['as' => 'manage.wallet', 'uses'=>'ManageController@wallet']);
  
   

   Route::group(['prefix'=>'users'],function(){
     Route::get('/', ['as' => 'manage.users', 'uses'=>'UserController@index']);
     Route::post('create', ['as' => 'manage.users.create', 'uses'=>'UserController@create']);
     Route::get('{id}/show', ['as' => 'manage.users.show', 'uses'=>'UserController@show']);
     Route::get('approve/{id}', ['as' => 'manage.users.approve', 'uses'=>'UserController@approve']);
   });

   Route::group(['prefix'=>'products'],function(){
     Route::get('/', ['as' => 'manage.products', 'uses'=>'ProductController@index']);
     Route::post('/', ['as' => 'manage.products', 'uses'=>'ProductController@view_csv']);
     Route::post('import', ['as' => 'manage.products.import', 'uses'=>'ProductController@upload_products_csv']);
     Route::get('create', ['as' => 'manage.products.create', 'uses'=>'ProductController@create']);
     Route::post('create', ['as' => 'manage.products.store', 'uses'=>'ProductController@store']);
     Route::get('{id}/edit', ['as' => 'manage.products.edit', 'uses'=>'ProductController@edit']);
     Route::post('update/{id}', ['as' => 'manage.product.update', 'uses'=>'ProductController@update']);
     Route::get('delete/{id}', ['as' => 'manage.products.delete', 'uses'=>'ProductController@delete']);
     Route::post('multidelete', ['as' => 'manage.products.multidelete', 'uses'=>'ProductController@multidelete']);


     Route::get('{id}/attributes', ['as' => 'manage.products.attributes', 'uses'=>'ProductController@add_attributes']);
     Route::post('attributes/store/{id}', ['as' => 'manage.products.attributes.store', 'uses'=>'ProductController@store_attributes']);
     Route::get('{id}/attributes/list', ['as' => 'manage.products.attributes.list', 'uses'=>'ProductController@list_attributes']);
     Route::get('{id}/attributes/{attribute_id}/edit', ['as' => 'manage.products.attributes.edit', 'uses'=>'ProductController@edit_attributes']);
     Route::post('{id}/attributes/{attribute_id}/update', ['as' => 'manage.products.attributes.update', 'uses'=>'ProductController@update_attributes']);
     Route::get('{id}/attributes/{attribute_id}/delete', ['as' => 'manage.products.attributes.delete', 'uses'=>'ProductController@delete_attributes']);
 
   });

   Route::group(['prefix'=>'category'],function(){
     Route::get('/', ['as' => 'manage.category', 'uses'=>'CategoryController@index']);
     Route::get('create', ['as' => 'manage.category.create', 'uses'=>'CategoryController@create']);
     Route::post('create', ['as' => 'manage.category.store', 'uses'=>'CategoryController@store']);
     Route::get('{cid}/edit', ['as' => 'manage.category.edit', 'uses'=>'CategoryController@edit']);
     Route::post('update/{id}', ['as' => 'manage.category.update', 'uses'=>'CategoryController@update']);
     Route::get('delete/{id}', ['as' => 'manage.category.delete', 'uses'=>'CategoryController@delete']);
     Route::get('{id}/subcategory/list', ['as' => 'manage.category.subcategory.list', 'uses'=>'CategoryController@list_subcategory']);
     Route::get('{id}/subcategory/create', ['as' => 'manage.category.subcategory.create', 'uses'=>'CategoryController@create_subcategory']);
     Route::post('{id}/subcategory/store', ['as' => 'manage.category.subcategory.store', 'uses'=>'CategoryController@store_subcategory']);
     Route::get('{cid}/subcategory/{id}/edit', ['as' => 'manage.category.subcategory.edit', 'uses'=>'CategoryController@edit_subcategory']);
     Route::get('{cid}/subcategory/{id}/delete', ['as' => 'manage.category.subcategory.delete', 'uses'=>'CategoryController@delete_subcategory']);
     Route::post('{cid}/subcategory/{id}/update', ['as' => 'manage.category.subcategory.update', 'uses'=>'CategoryController@update_subcategory']);
   });

});


Route::group(['middleware' => 'auth'], function () {
    Route::get('/plan/{plan}', 'PlansController@show');
    Route::get('/braintree/token', 'BraintreeTokenController@token');
    Route::post('/subscribe', 'SubscriptionsController@store');
    Route::get('checkout','BasketController@checkout');
    Route::post('proceed_checkout','BasketController@proceed_checkout');
    Route::get('plans', 'PlansController@index');

});


Route::get('manager/register', ['as' => 'manager.register', 'uses'=>'UserController@register_manager']);
Route::post('manager/register', ['as' => 'manager.register', 'uses'=>'UserController@store_manager']);


